# 本リポジトリについて
Open AI GymでPython版Robocodeのロボットを強化学習するためのリポジトリです。

## 事前準備
1. venv が使用できる環境にする。（Pythonインストール）
1. 本リポジトリをローカル環境にダウンロードする。 
  ダウンロード先のディレクトリで以下のコマンドを実行する。
  ```
  git clone https://gitlab.com/kazuki0819kazuki/ai_gym_env.git
  ```
1. ダウンロードしたvenv環境を取り込む。
  venv freeze

1. pipインストールを実行し、元のvenv環境でインストールしていたパッケージを復元する。
  pip install

## 環境起動方法(win)
  docker_run.batをクリック

## Docker

# 起動コマンド
### Dockerfile
#### DockerfileによるImage更新コマンド
docker build . -t ai_gym_ubuntu18:latest

# 参考
## Open AI Gym
### 公式

https://gym.openai.com/

### GitHub

https://github.com/openai/gym

## Robocode

### 公式
https://robocode.sourceforge.io/

### RoboWiki

https://robowiki.net/wiki/Main_Page

### API一覧

http://www.solar-system.tuis.ac.jp/Java/robocode_api/

## Python版Robocode
## GitHub

https://github.com/turkishviking/Python-Robocode
